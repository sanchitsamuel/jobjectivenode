import java.util.*;

public class Main {
    public static void main (String args[]) {
        // main function
        SinglyLinkedList singlyLinkedList = new SinglyLinkedList();
        for (int i = 0; i <= 10; i++)
            singlyLinkedList.insertAtTheEnd(i);
        //singlyLinkedList.printTillEnd();
        if (singlyLinkedList.deleteElement(2))
            System.out.println("Deletion successfull. ");
        singlyLinkedList.deleteElement(20);
        singlyLinkedList.deleteElement(0);
        //singlyLinkedList.printTillEnd();
        if (singlyLinkedList.insertAtTheStart(0))
            System.out.println("Insertion successfull. ");
        //singlyLinkedList.printTillEnd();
        singlyLinkedList.insertAfter(2, 1);
        //singlyLinkedList.printTillEnd();
        singlyLinkedList.rPrintTillEnd(singlyLinkedList.getStart());
        if (singlyLinkedList.rHasElement(10, singlyLinkedList.getStart()))
            System.out.println("Element Found. ");
        else
            System.out.println("Element not found. ");
        int position = singlyLinkedList.rElementLocation(singlyLinkedList.counterInit(), 0, singlyLinkedList.getStart());
        if (position != -1) {
            position++;
            System.out.println("Element's position "+ position+". ");
        }
    }
}