 class SinglyLinkedList {
    private Node start = null;
    private Node end = null;
    
//    public SinglyLinkedLis() {
//        start = null;
//        end = null;
//    }

    public Node getStart() {
        if (start != null)
            return start;
        else
            return null;
    }
    
    public int counterInit() {
        return 0;
    }
    
    public Node getLastNode() {
        if (start == null)
            return null;
            
        Node temp;
        temp = start;
        while (temp.getNext() != null) 
            temp = temp.getNext();
        return temp;
    }
    
    public int getElementCount() {
        if (start == null)
            return 0;
        int count = 0;
        Node temp;
        temp = start;
        while (temp.getNext() != null) {
            count++;
            temp = temp.getNext();
        }
        return count+1;
    }
    
    public boolean rHasElement(int d, Node node) {
        //Node temp;
        //temp = node;
        boolean status;
        if (node.getData() == d) {
            return true;
        } else {
            if (node.getNext() != null) {
                status = rHasElement(d, node.getNext());
                if (status)
                    return true;
            } else { return false; }
        }
        return false;
    }
    
    public int rElementLocation(int counter, int d, Node node) {
        int pos = -1;
        int count = counter;
        if (rHasElement(d, getStart())) {
            if (node.getData() == d) {
                return count;
            } else {
                if (node.getNext() != null) {
                    count++;
                    pos = rElementLocation(count, d, node.getNext());
                    if (pos != -1)
                        return pos;
                } else { return -1; }
            }
        } else { return -1; }
        return -1;
    }
    
    public boolean hasElement(int d) {
        if (start == null)
            return false;
        int size = 0;
        size = getElementCount();
        Node temp;
        temp = start;
        for (int i = 0; i < size; i++) {
            if (temp.getData() == d)
                return true;
            else
                temp = temp.getNext();
        }
        return false;
    }
    
    public boolean insertAtTheEnd(int d) {
        if(!hasElement(d)) {
            Node newNode = new Node(d);
            Node last = getLastNode();
            if (last == null) {
                start = newNode;
                end = newNode;
                return true;
            } else {
                last.setNext(newNode);
                end = newNode;
                return true;
            }
        } else {
            System.out.println("Element " + d +" already exists.");
            return false;
        }
    }
    
    public boolean insertAtTheStart(int d) {
        if(!hasElement(d)) {
            Node next = start;
            Node newNode = new Node(d, next);
            start = newNode;
            return true;
        } else {
            return false;
        }
    }
    
    public boolean insertAfter(int d, int p) {
        if (hasElement(p)) 
            if(!hasElement(d)) {
                Node temp;
                temp = start;
                while (temp.getNext() != null) {
                    if (temp.getData() == p) {
                        Node newNode = new Node(d, temp.getNext());
                        temp.setNext(newNode);
                        return true;
                    } else
                        temp = temp.getNext();
                }
                return false;
            } else 
                return false;
        else
            return false;
    }
    
    public boolean deleteElement(int d) {
        if (hasElement(d)) {
            Node temp;
            temp = start;
            while (temp.getNext() != null) {
                if (temp.getData() == d) {
                    Node toDelete = temp.getNext();
                    temp.setData(temp.getNext().getData());
                    temp.setNext(temp.getNext().getNext());
                    //delete toDelete;
                    return true;
                } else {
                    temp = temp.getNext();
                }
            }
            return false;
        } else {
            System.out.println("Cannot delete '" + d + "'. Element not found.");
            return false;
        }
        
    }
    
    public void printCurrentNodeData(Node node) {
        System.out.print(" " + node.getData() + " ");
    }
    
    public void printTillEnd() {
        Node temp;
        temp = start;
        while (temp.getNext() != null) {
            printCurrentNodeData(temp);
            System.out.print("> ");
            temp = temp.getNext();
        }
        printCurrentNodeData(temp);
        System.out.print("> ");
        System.out.print("End");
        System.out.println();
    }
    
    public void rPrintTillEnd(Node node) {
        Node temp;
        temp = node;
        printCurrentNodeData(temp);
        System.out.print("> ");
        if (temp.getNext()  != null)
            rPrintTillEnd(temp.getNext());
        else {
            System.out.print("End");
            System.out.println();
        }
    }
 }