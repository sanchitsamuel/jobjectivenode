 class Node {
    private int data;
    private Node next;
    
    public Node(int d) {
        this.data = d;
        this.next = null;
    }
    
    public Node(int d, Node addr) {
        this.data = d;
        this.next = addr;
    }
    
    public Node getNext() {
        if (this.next != null)
            return this.next;
        else
            return null;            
    }
    
    public void setNext(Node addr) {
        this.next = addr;
    }
    
    public int getData() {
        return this.data;
    }
    
    public void setData(int d) {
        this.data = d;
    }
 }